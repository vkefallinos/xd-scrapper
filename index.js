const handler = require('serve-handler');
const http = require('http');
const fs = require('fs-extra');
const puppeteer = require('puppeteer');
function createComponentFiles(components) { 
  const compTemplate = ({name, css}) => (
`
import React from "react";
import styled from "styled-components"

const ${name} = styled.div\`
${css}
\`
export default ${name}
`
) 
  for (let comp of components) {
    if(comp.name.endsWith("Component"))
    fs.outputFileSync(`./components/${comp.name}.js`, compTemplate(comp)) 
  }
  console.log("created Files")
}
var findDbFiles = function(dir, filelist) {
  var files = fs.readdirSync(dir);
  filelist = filelist || [];
  files.forEach(function(file) {
    if (fs.statSync(dir + file).isDirectory()) {
      filelist = findDbFiles(dir + file + '/', filelist);
    }
    else if(file === "db.js") {
      console.log(file)
      filelist.push(dir +file);
    }
  });
  return filelist;
};
function timeout(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
const server = http.createServer((request, response) => {
  // You pass two more arguments for config and middleware
  // More details here: https://github.com/zeit/serve-handler#options
  return handler(request, response);
})
 
server.listen(4000, async () => {
  const dbFiles = findDbFiles("./Mockups/")  
  const dbFile = dbFiles[0]
  console.log(dbFiles)
  const browser = await puppeteer.launch({headless: false});
  const page = await browser.newPage();

  // for (let dbFile of dbFiles) {
    fs.copyFileSync(dbFile, `./Scraper/dist/db.js`)
    page.setViewport({width: 4000, height: 1800})
    await page.goto('http://localhost:4000/Scraper/', { waitUntil: 'networkidle2' });
    // console.log("loadMargins")
    // await page.evaluate(loadMargins)
    // await timeout(10000)  
    console.log("click right")
    await page.click(".bp3-tree-node-label", {
      button: "right"
    })
    await timeout(1000)
    console.log("click getCss")
    await page.click(".bp3-menu-item")
    await timeout(3000)
    console.log("getComponents")
    while (true){
      await timeout(3000)
      const components = await page.evaluate(getComponents)
      console.log(components)
      createComponentFiles(components)

    }
    browser.close()
  // }
  console.log('Running at http://localhost:4000');
});


const getComponents = () => {
  var components = []
  for (var i in localStorage) {
    if (typeof localStorage[i] === "string") {
      components.push({
        name: i,
        css: localStorage[i]
      })
    }
  }
  return components
}
const loadMargins = () => {
  var simulateClick = function (elem) {
    // Create our event (with options)
    var evt = new MouseEvent('click', {
      bubbles: true,
      cancelable: true,
      view: window
    });
    // If cancelled, don't dispatch our event
    var canceled = !elem.dispatchEvent(evt);
  };
  var simulaterClick = function (elem) {
    // Create our event (with options)
    var evt = new MouseEvent('click', {
      bubbles: true,
      cancelable: true,
      view: window
    });
    // If cancelled, don't dispatch our event
    var canceled = !elem.dispatchEvent(evt);
  };
  var simulateOver = function (elem) {
    // Create our event (with options)
    var evt = new MouseEvent('mouseover', {
      bubbles: true,
      cancelable: true,
      view: window
    });
    // If cancelled, don't dispatch our event
    var canceled = !elem.dispatchEvent(evt);
  };
  function mover(elem) {
    setTimeout(() => {
      console.log("over", elem)
      simulateOver(elem)
      
    },100)
    
  }
  function cl(elem) {
    setTimeout(() => {
      console.log("click", elem)
      simulateClick(elem)
      var elems = elem.querySelectorAll("*")
      for (e in elems) {
        mover(elems[e])
      }
    },100)
  }
  function rcl(elem) {
    setTimeout(() => {
      console.log("click", elem)
      simulaterClick(elem)
      var elems = elem.querySelectorAll("*")
      for (e in elems) {
        mover(elems[e])
      }
    },100)
  }
  var elements = document.querySelectorAll(".container svg")
  for (var el in elements) {
    var elem = elements[el]
    cl(elem)
  }
  setTimeout(()=>{},1000)
  cl(document.querySelector(".bp3-tree-node-label"))
  
}